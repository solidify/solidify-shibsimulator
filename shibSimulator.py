import csv
import json
import logging
import os
import requests
import sys
from flask import Flask, request, render_template, send_from_directory, Response

app = Flask(__name__)
app.logger.addHandler(logging.StreamHandler(sys.stdout))
app.logger.setLevel(logging.DEBUG)
authorization_server_url = os.getenv('AUTHORIZATION_SERVER_URL')
if authorization_server_url is None:
    authorization_server_url = 'http://host.docker.internal:16100'


def load_users_file():
    users = []
    with open(os.path.dirname(__file__) + '/config/users.csv') as users_file:
        csv_reader = csv.reader(users_file, delimiter=',')
        for row in csv_reader:
            user = {
                "username": row[0],
                "password": row[1],
                "unique_id": row[2],
                "mail": row[3],
                "surname": row[4],
                "given_name": row[5],
                "home_organization": row[6]
            }
            users.append(user)
    return users

def isEmpty (str):
    return not (str and str.strip())

users = load_users_file()


def find_user(username):
    for user in users:
        if user['username'] == username:
            return user

def proxify_shiblogin(url, user):
    outcoming_headers = {
                'uniqueid': user['unique_id'],
                'mail': user['mail'],
                'surname': user['surname'],
                'givenname': user['given_name'],
                'homeorganization': user['home_organization']
    }
    app.logger.info("url=" + url)
    app.logger.info("outcoming_headers=" + json.dumps(outcoming_headers))
    outcoming_headers.update(request.headers)
    response = requests.get(url,headers=outcoming_headers, params=request.args, allow_redirects=False)
    return Response(response=response, status=response.status_code, headers=response.headers.items())

@app.route("/shiblogin", methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        user = find_user(request.form['username'])
        if user is None or request.form['password'] != user['password']:
            error = 'Invalid Credentials. Please try again.'
        else:
            return proxify_shiblogin(authorization_server_url + '/shiblogin',user)
    return render_template('login.html', error=error)
   
@app.route("/mfa-shiblogin", methods=['GET', 'POST'])
def mfa_login():
    error = None
    if request.method == 'POST':
        user = find_user(request.form['username'])
        if user is None or request.form['password'] != user['password']:
            error = 'Invalid Credentials. Please try again.'
        elif isEmpty(request.form['mfa']) or isEmpty(request.form['code']):
            error = 'Missing MFA Code. Please try again.'
        elif request.form['mfa'] !=  request.form['code']:
            error = 'Invalid MFA Code. Please try again.'
        else:
            return proxify_shiblogin(authorization_server_url + '/mfa-shiblogin',user)
    return render_template('mfa-login.html', error=error)

@app.route('/img/<path:path>')
def send_img(path):
    return send_from_directory('img', path)


@app.route('/favicon.ico')
def favicon():
    return ("not found", 404)


@app.route('/', defaults={'path': ''}, methods=["GET", "POST", "OPTIONS"])
@app.route('/<path:path>', methods=["GET", "POST", "OPTIONS"])
def catch_all(path):
    response = requests.request(
        method=request.method,
        url=authorization_server_url + request.path + '?' + request.query_string.decode(),
        headers=request.headers,
        data=request.get_data(),
        cookies=request.cookies,
        allow_redirects=False, stream=False)
    return Response(response=response, status=response.status_code, headers=response.headers.items(), direct_passthrough=True)


if __name__ == '__main__':
    app.run(use_reloader=True, debug=True)
